
<%inherit file="base.mako"/>

    <%block name="omix_main_content">

                <header class="imagescroll">
                    <div class="row vertical-align">
                        <div class="col-lg-12 text-center">
                            <h1 id="homeHeading">omicxview</h1>
                        </div>
                    </div>

                </header>
         

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p> Welcome to Omicxview a metabolic pathway visualisation tool.
                        To get started go to the visualisation page where you can select                        a range of pathways and match these with datasets. 
                        Perform actions such as view reactions between data and highlight                       metabolites which have significant difference between the two sample                    types.
                        A list contains information specifc to a metabolite, for example                        the p-value or t-statistic. Metabolites can be highlighted based                        on these values as well. 

                        Pathways are from Escher and can be cited with: Zachary A. King, Andreas Dräger, Ali Ebrahim, Nikolaus Sonnenschein, Nathan E. Lewis, and Bernhard O. Palsson (2015) Escher: A web application for building, sharing, and embedding data-rich visualizations of biological pathways, PLOS Computational Biology 11(8): e1004321. doi:10.1371/journal.pcbi.1004321 </p>
                
                </div>
            </div>

    </section>



    <aside class="bg-dark">
        <div class="container text-center">
            <div class="call-to-action">
                % if not view.logged_in_user_id:    
                    <h2>Register to start building your visualisations.</h2>
                    <a href="auth/register" class="btn btn-default btn-xl sr-button">Register</a>

                % else:
                    <h2>Start building your visualisations.</h2>
                    <a href="/vis/pathway_viewer" class="btn btn-default btn-xl sr-button">go!</a>

                % endif
            </div>
        </div>
    </aside>

    </%block>


