<%inherit file="base.mako"/>

        <%block name="omix_main_content">
                <div class="header-content" id="miniheader">
                    <div class="header-content-inner">
                        <h1 id="homeHeading">my page</h1>
                    </div>
                </div>
        
            % if error:
                <div class="errormsg">
                    Error: ${error}
                </div>
            % endif

            % if view.logged_in_user_role['role'] == 'admin': 
                <div class='content_wrapper'>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                <h3 class="mypage"> Add group </h3>
                                <form action="/mypage/add_group" method="POST">
                                    <div class="form-group">
                                      <input type="text" name="group-add-name" required class="form-control" placeholder="group name">
                                    </div>
                                    <div class="form-group">
                                      <input type="text" name="group-add-name-conf" required class="form-control" placeholder="confirm group name">
                                    </div>
                                    
                                <button type="submit" class="btn btn-danger left-float">Add group</button>
                              </form>
                        </div>

                        <div class="col-lg-4 text-center">
                            <h3 class="mypage"> Add user to group </h3>

                                <form action="/mypage/add_user_to_group" method="POST">
                                    <div class="form-group">
                                      <input type="text" name="add-user-to-group-name" required class="form-control" placeholder="group name">
                                    </div>

                                    <div class="form-group">
                                      <input type="email" name="add-user-to-group-email" required class="form-control" placeholder="users email">
                                    </div>
                                    <div class="form-group">
                                      <input type="email" name="add-user-to-group-email-conf" required class="form-control" placeholder="confirm users email">
                                    </div>
                                <button type="submit" class="btn btn-danger left-float">Add user</button>
                              </form>
                        </div>

                            <div class="col-lg-4 text-center">

                            <h3 class="mypage">Remove user from group </h3>

                                <form action="/mypage/remove_user_from_group" method="POST">
                                    <div class="form-group">
                                      <input type="text" name="remove-user-from-group-name" required class="form-control" placeholder="group name">
                                    </div>

                                    <div class="form-group">
                                      <input type="email" name="remove-user-from-group-email" required class="form-control" placeholder="user to remove's email">
                                    </div>
                                    <div class="form-group">
                                      <input type="email" name="remove-user-from-group-email-conf" required class="form-control" placeholder="confirm user to remove's email">
                                    </div>
                                <button type="submit" class="btn btn-danger left-float">Remove user</button>
                              </form>
                            </div>

                        </div>
                    </div>

                </div>
            % endif

            <div class='content_wrapper'>

             <div class="row">
                <div class="col-lg-5 col-lg-offset-1">
           
                    <h3 class="mypage leftpadd"> My groups </h3> <br>
                    <button type="button" id="hidemygroups" class="btn btn-info leftpadd">Hide my groups </button>

                    <table class="table table-striped" id="mygroups-table">
                        <thead>
                            <tr>
                                <th > Group Name </th>
                                <th > Number of members </th>
                                <th > Access </th>
                            </tr>
                        </thead>
                    
                        <tbody>
                            % if groups == None:
                                <tr>
                                    <td> No groups. </td><td></td><td></td>
                                </tr>
                            % else:
                                % for g in groups:
                                    <tr>
                                        <td> ${g[0]} </td><td></td><td></td>
                                    </tr>
                                % endfor 
                            % endif
                        </tbody>
                    </table>
                </div>
 
                <div class="col-lg-5">
 
                    <h3 class="mypage leftpadd"> My dictionaries </h3> <br>
                    <button type="button" id="hidemygroups" class="btn btn-info leftpadd">Hide my dictionaries </button>

                    <table class="table table-striped" id="mydicts-table">
                        <thead>
                            <tr>
                                <th > Dictionary Name </th>
                                <th > Number of identified metabolites </th>
                                <th > Access </th>
                            </tr>
                        </thead>
                    
                        <tbody>
                            % if dictionaries == None:
                                <tr>
                                    <td> No dictionaries. </td><td></td><td></td>
                                </tr>
                            % else:
                                % for d in dictionaries:
                                    <tr>
                                        <td> ${d} </td><td></td><td></td>
                                    </tr>
                                % endfor
                            % endif
                        </tbody>
                    </table>
                </div>
           

        </%block>
