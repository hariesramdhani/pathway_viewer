<%inherit file="base.mako"/>

        <%block name="omix_main_content">
                <div class="header-content" id="miniheader">
                    <div class="header-content-inner">
                        <h2 id="homeHeading">My data </h2>
                    </div>
                </div>     
            
            <link href=${view.web_path + "/public/css/vis/modal.css"} rel="stylesheet">

            <div id="error-modal" class="modal fade" tabindex="-1" role="dialog">
                <div  class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title modal-bold">Dataset errors </h4>
                        </div>
                        <div class="modal-body">
    
                        % if id_errors:
                            <div class="row modal-bold">
                                Unmapped metabolite identifiers
                            </div>

                            % for err in id_errors['id_errors']:
                                <div class="row">
                                    <div class="col-md-4 col-md-offset-2 info-col">
                                        ${err}
                                    </div>
                                </div>
                            % endfor

                            <div class="row modal-bold">
                                Non numeric expression values
                            </div>
    
                            % for err in id_errors['value_errors']:
                                <div class="row">
                                    <div class="col-md-4 label-col">
                                        ${err}
                                    </div>
                                    <div class="col-md-8 info-col">
                                        ${id_errors['value_errors'][err]}
                                    </div>
                                </div>
                            % endfor

                        % endif

                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            % if error:
                <div class="errormsg">
                    Error: ${error}
                </div>
            % endif

            % if id_information:
                <script type="text/javascript">
                    // If there are errors we want to show these to the user
                    $(window).on('load',function(){
                        $('#error-modal').modal('show');
                    });
                </script>
            % endif

            <div class='content_wrapper'>

                <p class="help"> To get started with creating a visualisation, you'll need to choose a dataset and a pathway.<br>
                  Either upload new data or choose from your existing pathways and datsets below.</p>

                <div class="container">
                    <div class="row">

                        <div class="col-lg-8 col-lg-offset-2">

                            <h3 class="mypage leftpadd"> Add Dataset </h3>
                           
                            <form action="" method="POST" accept-charset="utf-8" enctype="multipart/form-data" name="upload_dataset">

                                <input type="text" class="form-control" name="dataset_filename" required placeholder="Dataset name">

                                <div class="form-group">
                                % for dataset_type in supported_dataset_types:
                                        <label class="radio-inline leftpadd">
                                            <input type="radio" name="dataset_type" id=${dataset_type} value=${dataset_type} checked> ${dataset_type}
                                        </label>
                                % endfor
                                </div>
                                <div class="form-group">
                                    % for raw_metabolite_id_type in supported_id_types:
                                        <label class="radio-inline leftpadd">
                                            <input type="radio" name="raw_metabolite_id_type" id=${raw_metabolite_id_type} value=${raw_metabolite_id_type} checked> ${raw_metabolite_id_type.replace("_", " ")}
                                        </label>
                                    %endfor
                                </div>

                                <div class="form-group">
                                    <label class="radio-inline leftpadd">
                                        <input type="radio" name="dataset_options" id="user_select" value="user" checked> just me
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" name="dataset_options" id="group_select" value="group"> group access
                                    </label>
                                </div>
                              
                                <input type="text" class="form-control" name="dataset_group_name" placeholder="Group name (leave blank if you selected 'Just me')">
                    
                                <textarea class="form-control" rows="5" name="notes" placeholder="notes"></textarea>

                                <div class="form-group">
                                    <label class="radio-inline leftpadd">
                                        <input type="radio" name="access_options" id="user_select" value="t" checked> private
                                    </label>

                                    <label class="radio-inline">
                                        <input type="radio" name="access_options" id="group_select" value="f"> public
                                    </label>
                                </div>


                              <div class="form-group">
                                <input type="file" name="dataset_upload" id="dataset_upload" value=''>
                              </div>

                            <button type="submit" class="btn btn-danger leftpadd">Submit</button>
                          </form>


                        </div>



                    </div>
                </div>
            </div>
    
    
    <hr class="primarylong">


    <div id="datadiv" style="display: none;">
        ${json_datasets}
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Dataset Information</h4>
                </div>
                <div class="modal-body">

                    <div class="panel panel-info">
                        <div class="panel-heading"> Dataset ID </div>
                        <div class="panel-body" id="view_ds_id">

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"> Dataset Name </div>
                        <div id="view_ds_title" class="panel-body">

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"> Dataset Privacy (public or private) </div>
                        <div id="view_ds_private" class="panel-body">
    
                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"> Your access levels (user or group) </div>
                        <div id="view_ds_access" class="panel-body">

                        </div>
                    </div>
                    <div class="panel panel-info">
                        <div class="panel-heading"> Further Notes </div>
                        <div id="view_ds_notes"class="panel-body">

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

 	<div class='content_wrapper'>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">

                <h3 class="mypage leftpadd"> My Datasets </h3> <br>
                <button type="button" id="hidemydatsets" class="btn btn-info leftpadd">Hide my datasets </button>

                <table class="table table-striped" id="mygroups-table">
                    <thead>
                        <tr>
                            <th > Dataset ID </th>
                            <th > Name </th>
                            <th > Availability </th>
                            <th > Access Type </th>
                            <th > View </th>  
                      </tr>
                    </thead>
		            <tbody>
                        % if datasets == None:
                            <tr>
                                <td> No datasets. </td><td></td><td></td>
                            </tr>
                        % else:
                            % for s in datasets:
                                <tr>
                                    <td> ${s} </td>
                                    <td> ${datasets[s]['name']} </td>
                                    <td> ${datasets[s]['private']} </td>
                                    <td> ${datasets[s]['owner_type']} </td>
                                    <td>
                                        <button type="button" id=${s} onClick="view_dataset(this.id)"  class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
                                            View dataset
                                        </button>
                                    </td>
                                </tr>
                            % endfor 
                        % endif
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script src=${view.web_path + "/public/js/dataset/dataset_modal_view.js"}></script> 
</%block>
