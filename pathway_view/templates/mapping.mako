<%inherit file="base.mako"/>

<%block name="omix_main_content">

    <link href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href=${view.web_path + "/css/mapping/table.css"} rel="stylesheet">
    <link href=${view.web_path + "/css/mapping/general.css"} rel="stylesheet">


    <div class="header-content" id="miniheader">
        <div class="header-content-inner">
            <h1 id="homeHeading">Identifier Mapping</h1>
        </div>
    </div>

    % if error:
        <div class="errormsg">
            Error: ${error}
        </div>
    % endif

    <div class='content_wrapper'>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">

                <div class="row">
                    <h3 class="mypage leftpadd"> New Mapping </h3>
                </div>

                <div class="row">
                    <form class="form-inline" action="" method="POST" accept-charset="utf-8" enctype="multipart/form-data" name="upload_dataset">
                        
                            % for id_type in id_types:
                                    <label class="leftpadd"> ${id_type} </label>
                                    <input class="checkbox" type="checkbox" value=${id_type} id=${id_type} name=${id_type} checked />
                            % endfor
                
                        <div class="form-group">
                            <input class="leftpadd" type="file" name="id-list" id="id-list" value=''>
                        </div>

                        <button type="submit" class="btn btn-danger">Submit</button>
                    </form>
                </div>

                <div class="row">
                    <h3 class="mypage leftpadd"> Results </h3>
                </div>

                <table class="display table table-striped" id="table-stats">
                   % if mapping_stats != None:
                            <thead>
                                <tr>
                                    <th> Name </th>
                                    <th> Value </th>
                                    <th> Percent </th>
                                </tr>
                            </thead>
                        <tbody>

                        % for row in mapping_stats:
                            <tr>
                                <td> ${row} </td>
                                <td> ${round(mapping_stats[row], 2)} </td>
                                <td> ${round((mapping_stats[row]/mapping_stats['Number of unique ids in file']) * 100, 2)} </td>
                            </tr>
                        % endfor
                    </tbody>

                    % endif 
                </table>


                <table class="display table table-striped" id="table">
                   % if mapped_ids != None:
                        <% count = 0 %>
                        % for row in mapped_ids:
                            % if count == 0: 

                                <thead>
                                    <tr>
                                        <th> Raw ID </th> 
                                    % for id_type in selected_id_types:
                                        <th> ${id_type} </th>
                                    % endfor
                                    </tr>
                                </thead>

                                <tbody>

                                % endif
                            <% count += 1 %>
                            <tr>
                                <td> ${row} </td>
                                % for id_type in selected_id_types:
                                    % if isinstance(mapped_ids[row], str):
                                        <td> <div class="table-row"> <div> </td>
                                    % else:
                                        <td> <div class="table-row"> ${', '.join(mapped_ids[row][id_type])} <div> </td>
                                    % endif
                                % endfor 
                            </tr>
                        % endfor 
                    % endif 
                </tbody>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.3.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.flash.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.27/build/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.3.1/js/buttons.print.min.js"></script>
 
    <script>
        $(document).ready(function(){
            $('#table').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'csv'
                ]
            } );
        } );

        $(document).ready(function(){
            $('#table-stats').DataTable(
            );
        } );

    </script>

</%block>

