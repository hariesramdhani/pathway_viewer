from pyramid.config import Configurator

#http://docs.pylonsproject.org/projects/pyramid/en/1.3-branch/narr/sessions.html
from pathway_view.controller.auth_controller import AuthController
from pathway_view.controller.dataset_controller import DatasetController
from pathway_view.controller.pathway_controller import PathwayController
from pathway_view.controller.group_controller import GroupController
from pathway_view.controller.vis_controller import VisController
from pathway_view.controller.metabolite_list_controller import MetaboliteListController
from pathway_view.controller.mapping_controller import MappingController


from wsgiref.simple_server import make_server

def init_routing(config):
    config.add_static_view('static', 'static', cache_max_age=3600)

    config.add_handler('root', '/', handler=AuthController, action='home')
    
    # General
    add_controller_routes(config, AuthController, 'home')
    
    # Auth
    add_controller_routes(config, AuthController, 'auth/logout')
    add_controller_routes(config, AuthController, 'auth/login')
    add_controller_routes(config, AuthController, 'auth/register')
    add_controller_routes(config, AuthController, 'auth/confirm_registration')

    # -------------------------------------------------------------------------- #
    # ----------------------- Page for help features ------------------------ #
    add_controller_routes(config, AuthController, 'auth/help')
    # -------------------------------------------------------------------------- #

    # Group and admin
    add_controller_routes(config, GroupController, 'mypage')

    # Data upload and data view (lists, datasets, pathways)
    add_controller_routes(config, DatasetController, 'datasets/index')
    add_controller_routes(config, PathwayController, 'pathways/index')
    add_controller_routes(config, MetaboliteListController, 'metabolite_lists/index')

    # Mapping page
    add_controller_routes(config, MappingController, 'mapping/index')
    
    
    # Visualisation
    add_limited_controller_routes(config, VisController, 'vis/pathway_viewer')

    # Ajax calls for the visualisation
    add_controller_routes(config, VisController, 'vis/get_dataset_data')
    add_controller_routes(config, VisController, 'vis/get_pathway_data')
    add_controller_routes(config, VisController, 'vis/get_metabolite_list_data')
    add_controller_routes(config, VisController, 'vis/get_metabolite_list_mapping_data')


    config.scan()


def add_controller_routes(config, ctrl, prefix):
    config.add_handler(prefix + 'ctrl_index', '/' + prefix, handler=ctrl, action=prefix)
    config.add_handler(prefix + 'ctrl_index/', '/' + prefix + '/', handler=ctrl, action=prefix)
    config.add_handler(prefix + 'ctrl', '/' + prefix + '/{action}', handler=ctrl)
    config.add_handler(prefix + 'ctrl/', '/' + prefix + '/{action}/', handler=ctrl)
    config.add_handler(prefix + 'ctrl_id', '/' + prefix + '/{action}/{id}', handler=ctrl)


def add_limited_controller_routes(config, ctrl, prefix):
    config.add_handler(prefix + 'ctrl_index', '/' + prefix, handler=ctrl, action=prefix)
    config.add_handler(prefix + 'ctrl_index/', '/' + prefix + '/', handler=ctrl, action=prefix)

def init_includes(config):
    config.include('pyramid_handlers')
    config.include('pyramid_mako')


# From https://github.com/mikeckennedy/python-for-entrepreneurs-course-demos
#def main(global_config, **settings):
if __name__ == '__main__':
    config = Configurator()
    # Static public js and css files 
    config.add_static_view(name='pathway_view_public', path='pathway_view:pathway_view_public')
    init_includes(config)
    init_routing(config)
    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 5000, app)
    server.serve_forever()
 
