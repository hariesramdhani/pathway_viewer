
import re
import csv
import json

# Headers for the tsv file
chebi_id = "ChEBI ID"
chebi_name = "ChEBI Name"
definition = "Definition"
mass = "Mass"
m_mass = "Monoisotopic Mass"
syno = "Synonyms"
kegg_c = "KEGG COMPOUND Database Links"
charge = "Charge"
secondary_id = "Secondary ChEBI ID"
formula = "Formulae"
inchi_id = "InChi Key"
inchi_str = "InChi String"
date_mod = "Last Modified"
bigg_ids = "BiGG IDs"
metanetx_name = "MetaNetX ID"
metanet_pos = 14

parsed_db_dir = "parsed_db_outputs/"
db_dump_dir = "database_dumps/"
base_dir = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/"

save_path = base_dir + parsed_db_dir + "chebi_info.tsv"
sdf_path = base_dir + db_dump_dir + "ChEBI_complete_3star_10072017.sdf"


bigg_file_path = base_dir + db_dump_dir + "bigg_models_metabolites_10072017.txt"

metanetx_file_path = base_dir + db_dump_dir + "metanetx_chem_prop_10072017.tsv"

metanetx_chebi_file_path = base_dir + db_dump_dir +  "metanetx_chem_xref_10072017.tsv"

json_root_path = base_dir + db_dump_dir + "escher_maps_03072017/"


save_path_metanetx = base_dir + parsed_db_dir + "neutral_inchi_to_metanetx.tsv"

save_path_bigg = base_dir + parsed_db_dir + "neutral_inchi_to_bigg.tsv"

output_bigg_paths = base_dir + parsed_db_dir + "neutral_inchi_to_bigg_paths.tsv"

output_bigg_name = base_dir + parsed_db_dir + "neutral_inchi_to_bigg_name.tsv"

save_map_path = base_dir + parsed_db_dir + "mapping_metrics.tsv"


output_metanetx = open(save_path_metanetx, 'w')
output_metanetx = csv.writer(output_metanetx, delimiter='\t')
output_metanetx.writerow(['neutral_inchi_str', 'metanetx_id'])

output_bigg = open(save_path_bigg, 'w')
output_bigg = csv.writer(output_bigg, delimiter='\t')
output_bigg.writerow(['neutral_inchi_str', 'bigg_id'])

output_bigg = open(save_path_bigg, 'w')
output_bigg = csv.writer(output_bigg, delimiter='\t')
output_bigg.writerow(['neutral_inchi_str', 'bigg_id'])

output_bigg_paths = open(output_bigg_paths, 'w')
output_bigg_paths = csv.writer(output_bigg_paths, delimiter='\t')
output_bigg_paths.writerow(['neutral_inchi_str', 'bigg_paths'])

output_bigg_name = open(output_bigg_name, 'w')
output_bigg_name = csv.writer(output_bigg_name, delimiter='\t')
output_bigg_name.writerow(['neutral_inchi_str', 'bigg_paths'])

err_path = "err.tsv"

err = open(err_path, 'w')
err_writer = csv.writer(err, delimiter='\t')
err_writer.writerow(['bigg', 'metanetx', 'inchi', 'err'])

bigg_id = "BiGG ID"
bigg_chebi = "CHEBI"

meta_chebi_key = 'chebi'
meta_bigg_key = 'bigg'
meta_hmdb_key = 'hmdb'
meta_bio_cyc_key = 'metacyc'

secondary_id_map = {}
inchi_str_map = {}

json_paths = ["Ee_coli_core.Core metabolism.json",
                "EiJO1366.Central metabolism.json",
                "EiJO1366.Fatty acid beta-oxidation.json",
                "EiJO1366.Fatty acid biosynthesis (saturated).json",
                "EiJO1366.Nucleotide and histidine biosynthesis.json",
                "EiJO1366.Nucleotide metabolism.json",
                "SiMM904.Central carbon metabolism.json",
                "HRECON1.Amino acid metabolism (partial).json",
                "HRECON1.Carbohydrate metabolism.json",
                "HRECON1.Glycolysis TCA PPP.json",
                "HRECON1.Inositol retinol metabolism.json",
                "HRECON1.Tryptophan metabolism.json"]


def perform_mapping(bigg_id_dict, bigg_to_metanetx, metanetx_to_inchi):
    # For each bigg identifier we want to assign a unique chebi which is gotten from
    # the neutral form of the inchi string
    mapping_dict = {}
    no_counts = {'metanetx': 0, 'inchi': 0}
    for bigg_id in bigg_id_dict:
        tmp = {}
        # Initialise the dictionary so when it comes to writing it to file we
        # don't need to do any checks
        tmp['metanetx_id'] = ""
        tmp['inchi_str_neutral'] = ""
        tmp['inchi_str_raw'] = ""
        try:
            metanetx_id = bigg_to_metanetx[bigg_id]
            tmp['metanetx_id'] = metanetx_id
            neutral_inchi_str = ""
            # Try and get the extra mappings from the other mappings available in metanetx
            try:
                neutral_inchi_str = metanetx_to_inchi[metanetx_id]['inchi_str_neutral']
                tmp['inchi_str_neutral'] = neutral_inchi_str
                """
                Save this to the TSV file on the inchi string
                """
                output_bigg.writerow([neutral_inchi_str, bigg_id])
                output_metanetx.writerow([neutral_inchi_str, metanetx_id])
                output_bigg_name.writerow([neutral_inchi_str, bigg_name_dict[bigg_id]])
                for path in bigg_path_dict[bigg_id]:
                    output_bigg_paths.writerow([neutral_inchi_str, path])
                tmp['inchi_str_raw'] = metanetx_to_inchi[metanetx_id]['inchi_str_raw']
            except:
                # output_no_inchi.writerow([metanetx_id])
                # output_bigg_name.writerow([neutral_inchi_str, bigg_name_dict[bigg_id]])
                # for path in bigg_path_dict[bigg_id]:
                #     output_no_inchi.writerow([bigg_id, path, ])
                print(bigg_id, bigg_name_dict[bigg_id], metanetx_id, neutral_inchi_str)
                no_counts['inchi'] += 1
                err_writer.writerow([bigg_id, bigg_name_dict[bigg_id], metanetx_id])

                print("Unable to map inchi from bigg, bigg id: ", bigg_id, "metanetx id: ", metanetx_id)
        except:
            err_writer.writerow([bigg_id, "", "",
                                 "Unable to map metanetx from bigg id"])
            print("Unable to map metanetx from bigg, bigg id: ", bigg_id)
            no_counts['metanetx'] += 1
        # Add the mappings to the dict
        mapping_dict[bigg_id] = tmp

    print(no_counts)
    print("% mapped: ", (len(bigg_id_dict)- no_counts['metanetx'] - no_counts['inchi'])/ len(bigg_id_dict))
    return mapping_dict

# Make it global to access later
bigg_path_dict = {}
bigg_name_dict = {}

def parse_bigg_paths():
    all_nodes = []

    # collect each of the identifiers from the pathways
    for path in json_paths:
        actual_path = json_root_path + path[1:]
        with open(actual_path, 'r') as json_file:
            data = json.load(json_file)
            for node_id in data[1]['nodes']:
                node = data[1]['nodes'][node_id]
                try:
                    bigg_id = node['bigg_id']
                    # We need to trim the bigg id so that it becomes the universal identifier
                    # The last two characters are an _ and location id
                    bigg_id = bigg_id[:-2]
                    bigg_name_dict[bigg_id] = node['name']
                    all_nodes.append(bigg_id)
                    try:
                        pathways = bigg_path_dict[bigg_id]
                    except:
                        pathways = []
                    if path.split(".")[1] not in pathways:
                        pathways.append(path[0] + path.split(".")[1])
                    bigg_path_dict[bigg_id] = pathways
                except:
                    if node['node_type'] != 'multimarker' and node['node_type'] != 'midmarker':
                        print(node)

    print("num bigg ids in pathways:", len(bigg_path_dict))
    return bigg_path_dict


def read_metanetx_bigg(metanetx_bigg_file_path):
    meta_other_dict = {}
    # To make it quick we want to be able to quickly get the metanetx
    # for a bigg id.
    bigg_meta_dict = {}
    dup_count = 0
    # Want to itterate through the properties of the metanetx file which has the bigg identifers
    # Create a dictionary between bigg identifiers and metanetx ids
    with open(metanetx_bigg_file_path, "r") as f:
        meta_tsv = csv.reader(f, delimiter='\t')
        for row in meta_tsv:
            if row[0][0] != "#":
                meta_id = row[1]
                try:
                    other_id_key = row[0].split(":")[0]
                    other_id_val = row[0].split(":")[1]
                except:
                    safely_ignore = True
                if other_id_key == meta_bigg_key:
                    # Check if an entry already exists with that metanetX id if so it is a duplicate and we shall ignore
                    try:
                        other_meta = bigg_meta_dict[other_id_val]
                        print("Duplicate bigg ID entries for metanetx: ", meta_id, other_id_val, other_meta)
                        dup_count += 1
                    except:
                        bigg_meta_dict[other_id_val] = meta_id


    return bigg_meta_dict


def read_metanetx_inchi(metanetx_inchi_file_path):
    # compile the regex to be used
    pattern = re.compile("\/p[-+]\d")
    # Keep count of num of matches
    count_raw = 0
    count_entries = 0
    # Store the metanetx ids with corresponding inchi strings
    metanetx_inchi_dict = {}
    with open(metanetx_inchi_file_path, "r") as f:
        meta_tsv = csv.reader(f, delimiter='\t')
        for row in meta_tsv:
            if row[0][0] != "#":
                count_entries += 1
                meta_id = row[0]
                inchi_str_raw = row[5]
                if len(inchi_str_raw) > 5:
                    count_raw += 1
                    # Remove the charge substring from the inchi string
                    # If we already have a metanetx id which maps to that inchi string append
                    # this metanetx to the list.
                    inchi_str_neutral = pattern.sub("", inchi_str_raw)
                    # save the inchi string for the metanetx id
                    metanetx_inchi_dict[meta_id] = {'inchi_str_raw': inchi_str_raw,
                                                    'inchi_str_neutral': inchi_str_neutral}
    print("entries in metanetx with inchi_str: ", count_raw, "total number of entries in metanetx:", count_entries)
    return metanetx_inchi_dict

inchi_to_metanetx = read_metanetx_inchi(metanetx_file_path)
metanetx_bigg_mappings = read_metanetx_bigg(metanetx_chebi_file_path)
bigg_id_dict = parse_bigg_paths()
mapping_dict = perform_mapping(bigg_id_dict, metanetx_bigg_mappings, inchi_to_metanetx)

