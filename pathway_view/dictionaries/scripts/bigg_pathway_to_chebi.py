import json

from pathway_view.config import *

# We want to read in the pathway file and alter it to contain all the chebi's 
# associated with each molecule. This will make the file bigger but saves on
# having to make a second call. May need to change later.
pathway_path = "TCA_cycle_raw.json"

def parse_path(pathway_path):
    json_str = ""
    with open(pathway_path, 'r') as f:
        for line in f:
            json_str += line

    return json_str


def get_info_for_bigg(bigg_id, conn, cursor):
    sql = "select distinct ci.chebi_id, ci.charge, ci.mass, ci.name from chebi_mapping_to_other_id as co left join chebi_info as ci on co.chebi_id = ci.chebi_id where co.other_id = %s order by ci.chebi_id;"
    cursor.execute(sql, (bigg_id,))
    chebi_ids = cursor.fetchall()
    return chebi_ids

def add_chebi_to_path(path_json):
    conn = psycopg2.connect(psycopg2_conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    
    bigg_dict = {}
    # We want to go through each node in the pathway and get the bigg id
    # Turn this into a dict and fill out the chebi info associated with it
    # Don't want to do it for each node as there will be duplicates and want to 
    # minimise the info associated.

    for node_id in path_json[1]['nodes']:
        node = path_json[1]['nodes'][node_id]
        # Only want to find for metabolites
        if node['node_type'] == 'metabolite':
            bigg_dict[node['bigg_id']] = {}

    # Now want to itterate through each of the collected bigg ids and add chebi info
    # to each one.
    for bigg_id in bigg_dict:
        chebis = get_info_for_bigg(bigg_id, conn, cursor)
        # Want to get the chebi id with neutral charge (if one exists) 
        # Otherwise we do the one with the most smallest chebi id
        chosen_chebi = None

        chebis_parsed = []

        for chebi in chebis:
            # convert the numeric values to floats as from the DB comes with 
            # decimal prefix
            chebi[1] = float(chebi[1])
            chebi[2] = float(chebi[2])
            # To make it easier on the front end we want to convert it to a dict
            # with the names of the attributes
            tmp = {}
            tmp['chebi_id'] = chebi[0]
            tmp['charge'] = chebi[1]
            tmp['mass'] = chebi[2]
            tmp['name'] = chebi[3]
            
            # add it to the list of parsed chebis
            chebis_parsed.append(tmp)
            
            # if the chebi has 0 charge we want this to be the chosen chebi
            if chebi[1] == 0:
                chosen_chebi = tmp
            
        # If none have a 0 charge, choose the one with the smallest ID 
        # If there are none print out the bigg id
        if len(chebis) < 1:
            print(bigg_id)

        elif not chosen_chebi:
            chosen_chebi = chebis_parsed[0]

        bigg_dict[bigg_id]['chosen_chebi'] = chosen_chebi
        bigg_dict[bigg_id]['other_chebis'] = chebis_parsed    

    cursor.close()
    conn.close()

    # When dumping the json it gets messed up, so dump the dictionary separately
    return bigg_dict

def save_json(path_json, out_file_path, pathway_path):
    # to get over the issue of changing the json, we will just re read in the
    # origional json file and add the new dictionary as a string
    # need to remove the last bracket, the json is stored as a list of objects
    raw_json_str = parse_path(pathway_path)
    # dict as a string
    chebi_dict_str = json.dumps(path_json)
    # cat the two together
    new_json_path_str = raw_json_str[:-1] + "," + chebi_dict_str + "]"
    with open(out_file_path, 'w') as outfile:
        outfile.write(new_json_path_str)


# Run the commands
json_root_path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/pathway_data/raw/human/"

json_paths = ["RECON1.Glycolysis TCA PPP.json"] #["RECON1.Amino acid metabolism (partial).json", "RECON1.Carbohydrate metabolism.json", "RECON1.Glycolysis TCA PPP.json", "RECON1.Inositol retinol metabolism.json", "RECON1.Tryptophan metabolism.json"]

save_path = "/var/www/pathway_viewer/pathway_viewer/pathway_view/dictionaries/data/pathway_data/chebi_added/"

for path in json_paths:
    path_json = json.loads(parse_path(json_root_path + path))
    bigg_dict = add_chebi_to_path(path_json)
    save_json(bigg_dict, save_path + path, json_root_path + path)
