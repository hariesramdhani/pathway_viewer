#From https://github.com/mikeckennedy/python-for-entrepreneurs-course-demos/blob/master/12_user_accounts/blue_yellow_app_12/blue_yellow_app/infrastructure/cookie_auth.py

import hashlib

from datetime import timedelta
from pathway_view.config import *

# Set a name to view in the browser
auth_cookie_name = OMICXVIEW_COOKIE_AUTH_NAME
pre_salt = PRE_SALT
post_salt = POST_SALT

def set_auth(request, user_id):
    hash_val = __hash_text(user_id)
    
    # Do this to make it more difficult for people to guess an ID
    val = "{}:{}".format(user_id, hash_val)

    # Need to add the cookie at the very end of the request otherwise it doesn't
    # get added to the browser (some strange bug in pyramid)
    request.add_response_callback(lambda req, resp: __add_cookie_callback(
        req, resp, auth_cookie_name, val
    ))


def __hash_text(text):
    # This adds an extra level of protection around an ID by
    # adding some unknown values
    text = pre_salt + text + post_salt
    return hashlib.sha512(text.encode('utf-8')).hexdigest()


def __add_cookie_callback(_, response, name, value):
    response.set_cookie(name, value, max_age=timedelta(days=30))


def get_user_id_via_auth_cookie(request):
    if auth_cookie_name not in request.cookies:
        return None

    val = request.cookies[auth_cookie_name]
    parts = val.split(':')

    # If its not two parts then it has been tampererd with
    if len(parts) != 2:
        return None

    user_id = parts[0]
    hash_val = parts[1]
    hash_val_check = __hash_text(user_id)
    if hash_val != hash_val_check:
        print("Warning: Hash mismatch, invalid cookie value")
        return None

    try:
        uid = int(user_id)
        return uid
    except:
        return None


def logout(request):
    # Again to delete we need to call a lambda function to ensure that the
    # cookie is actually deleted from the browser
    request.add_response_callback(lambda req, resp: __delete_cookie_callback(
        resp, auth_cookie_name
    ))


def __delete_cookie_callback(response, name):
    response.delete_cookie(name)
