# General
import json

# Pyramid
import pyramid_handlers
from pyramid.renderers import render_to_response

# Config
from pathway_view.config import *

# Models
from pathway_view.model.auth import get_group_by_name
from pathway_view.model.metabolite_lists import MetaboliteListModel

# Controllers and helpers
import pathway_view.controller.helper.cookie_auth as cookie_auth
from pathway_view.controller.helper.suppress import suppress
from pathway_view.controller.base import BaseController


class MetaboliteListController(BaseController):

    def init_variables(self):
        self.lists = None
        self.error = None
        self.json_lists = None
        self.metabolite_header_list = METABOLITE_LIST_HEADER #None
        self.supported_metabolite_id_types = SUPPORTED_METABOLITE_ID_TYPES #None


    def to_dict(self):
        return self.__dict__


    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/metabolite_lists.mako',
                             request_method='GET',
                             name='metabolite_lists/index')    
    def index(self):
        # Similar to the dataset index page
        # returns a list of the lists a user has access to 
        # Model calls:
        #       Get all lists for user
        uid = self.logged_in_user_id

        if not uid:
            self.redirect('auth/login')
        
        list_model = MetaboliteListModel()

        # Update controller to contain all the metabolite lists for a user
        self.get_metabolite_lists_for_user(list_model, uid)
        
        return self.to_dict()


    # Updates the list controller to have either the lists or the errors
    # stored
    # Suppress means it can't be accessed by the web
    @suppress()
    def get_metabolite_lists_for_user(self, list_model, uid):
        lists = list_model.get_all_processed_metabolite_lists_for_user(uid)
        if isinstance(lists, str):
            self.error = lists
        else:
            self.lists = lists
            self.json_lists = json.dumps(lists)

    
    @pyramid_handlers.action(renderer=PROJ_PATH + 'templates/metabolite_lists.mako',
                             request_method='POST',
                             name='metabolite_lists/index')
    def add_metabolite_list(self):
        # loads the users list as a tsv file 
        # Metabolite id, abunance
        # 
        # Gets from the users form:
        #       list file
        #       collumns the user has selected
        #       list type (metabolite or pathway)
        #
        # Model calls:
        #       parse_list_metadata (parses the users input about the list)
        #       save_raw_list (save the raw file)
        #       process_raw_list (process the list)
        #       get_all_processed_lists for user (return the user with an updated list of lists)
        #
        uid = self.logged_in_user_id

        if not uid:
            self.redirect('auth/login')


        list_model = MetaboliteListModel()

        list_model.name = self.request.POST.get('filename')
        list_model.upload_file = self.request.POST.get('upload').file
        list_model.upload_filename = self.request.POST.get('upload').filename
        list_model.private = self.request.POST.get('access_options')
        list_model.owner_type = self.request.POST.get('owner_options')
        list_model.notes = self.request.POST.get('notes')
        list_model.group_name = self.request.POST.get('group_name')
        list_model.path = None
        list_model.owner_id = uid

        # Need to read in the users choices for matching the tsv header
        # and the options (e.g. p-value to col 1)
        col_dict = {}
        for i in range(0, len(METABOLITE_LIST_HEADER)):
            if METABOLITE_LIST_HEADER[i] == 'raw_metabolite_id_type':
                list_model.raw_metabolite_id_type = self.request.POST.get(METABOLITE_LIST_HEADER[i])
            else:
                try:
                    col_indicie = int(self.request.POST.get(METABOLITE_LIST_HEADER[i]))
                except:
                    col_indicie = None
                col_dict[METABOLITE_LIST_HEADER[i]] = col_indicie

        list_model.col_dict = col_dict


        # parse the metadata the user has uploaded and check this contains no errors
        success = list_model.parse_list_metadata()

        # update the controllers list of lists
        self.get_metabolite_lists_for_user(list_model, uid)

        # return the lists to the user and an error message
        if list_model.error:
            self.error = error
            return self.to_dict()
        
        # Save the raw file
        error = list_model.save_raw_list()
        
        if list_model.error:
            self.error = error
            return self.to_dict()

        # Process and save the metadata
        list_info = list_model.process_metabolite_list_metadata()

        if list_model.error:
            self.error = list_info
            return self.to_dict()

        # Process and save the data in the list
        list_data = list_model.process_raw_metabolite_list_data()

        if list_model.error:
            self.error = list_data
            return self.to_dict()

        # Update the controllers list of lists
        self.get_metabolite_lists_for_user(list_model, uid)

        return self.to_dict()
        
