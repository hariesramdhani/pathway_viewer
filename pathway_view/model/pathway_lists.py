import os
import uuid
import shutil
import datetime

from pathway_view.model.base import BaseModel
from pathway_view.config import *
from pathway_view.model.auth import get_group_by_name

import csv

PATH = '/var/www/pathway_viewer/pathway_viewer/pathway_view/uploads/lists/pathway'



class PathwayListModel(BaseModel):
    
    def __init__(self):
        self.path_id = None
        self.name = None
        self.error = None
        self.owner_id = None
        self.list_id = None

    def from_dict_upload(self, data_dict):
        self.name = str(data_dict.get('filename'))
        self.upload_file = data_dict.get('upload').file
        self.upload_filename = str(data_dict.get('upload').filename)
        self.private = str(data_dict.get('access_options'))
        self.owner_type = str(data_dict.get('owner_options'))
        self.notes = str(data_dict.get('notes'))
        self.group_name = str(data_dict.get('group_name'))
        # A dict of the collumn names and which collumn index they belong to
        self.col_dict = json.loads(str(data_dict.get('col_dict')))
        self.path = None
        self.error = False


    def parse_list_metadata(self):
        # Type:
        #       Matabolite or Pathway
        # Normal checks e.g. for Metabolite: 
        #   Has the user selected a collumn for:
        #       Metabolite Name
        #       ChEBI ID
        # Is there a file uploaded and they've supplied a name

        if not isinstance(self.name, str) or not isinstance(self.notes, str) or not NAME_REGREX.match(self.name):
            self.error = True
            return "There was an error with uploaded data."

        # Determine whether it is a pathway for a group or a user
        if self.owner_type == 'group':
            # From the authmodel
            group = get_group_by_name(self.group_name)
            if group == error_codes['name']:
                self.error = True
                return "Error with the uploaded group name"
            # set the owner_id to be the group id
            self.owner_id = group[ID]

        # Check that collumns have been selected

        return "success"


    def save_raw_list(self):
        # Saves the raw file that the user has uploaded
        # Saves as a tmp file and will be renamed once the list has been parsed
        # to /uploads/lists/type/name_listID.tsv
        filename = self.name
        input_file = self.upload_file

        # Check the user has provided a name and a file
        if not isinstance(filename, str):
            self.error = True
            return "There was an issue with the file or filename you uploaded, please try again."

        # File may throw an error so surround with try catch
        try:
            file_path = os.path.join(PATH, '%s.JSON' % filename)

            temp_file_path = file_path + '~'
            input_file.seek(0)

            with open(temp_file_path, 'wb') as output_file:
                shutil.copyfileobj(input_file, output_file)

            os.rename(temp_file_path, file_path)

            # update the path so that we can have access to the file
            self.path = file_path
            return "success"

        except:
            self.error = True
            return "There was an error parsing the file."



    def process_pathway_list_metadata(self):
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Test if there already exists a list with the same name
        sql = "SELECT * from pathway_lists where name = %s;"
        cursor.execute(sql,(self.name,))
        list_metadata = cursor.fetchall()

        if list_metadata:
            self.error = True
            # Need to delete the list that was added
            return "There already exists a list with that name, please choose a new name."

        sql = "INSERT into pathway_lists (name, owner_id, owner_type, raw_filepath, notes, private)  VALUES (%s, %s, %s, %s, %s, %s);"

        cursor.execute(sql,(self.name, self.owner_id, self.owner_type, self.path, self.notes, self.private,))

        sql = "SELECT * from pathway_lists where name = %s and owner_id = %s and owner_type = %s;"

        cursor.execute(sql,(self.name, self.owner_id, self.owner_type,))
        list_metadata = cursor.fetchall()
        cursor.close()
        conn.commit()
        conn.close()

        # check it has been added
        # add the ID to the list
        if len(list_metadata) != 1:
            self.error = True
            # remove the file
            os.remove(self.path)
            return "Wasn't able to add metadata."

        self.list_id = int(list_metadata[0][ID])
        return list_metadata[0]


    
    def process_pathway_list(self, path_id):
        # Save list in the database
        # 1. In pathway_list
        # 2. For Each row:
        #       An entry in: pathway_list_entry
        filepath = self.path
        list_id = int(self.list_id)

        # connect to db
        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Contains the indicies of the collumns to store
        col_dict = self.col_dict
        count = 0

        with open(filepath, 'r') as filein:
            filein = csv.reader(filein, delimiter='\t')

            # Read each row
            for row in filein:
                count += 1
                # If it is the first row we want to store the samples
                if count == 1:
                    num_cols = len(row)
                else:
                    # Stores the values after they have been converted to real numbers (or none)
                    row_dict = {'p_value': None, 'z_score': None, 'name': None, 'comments': None}
                    # Try to parse each of the collumns in the collumn dict
                    for i in col_dict:
                        if col_dict[i] != None:
                            # We need to add this to the data table
                            value = row[col_dict[i]]
                            # comments and name don't need to be converted to numeric values
                            if (i == 'name' or i == 'comments'):
                                val_processed = value
                            else:
                                # Do the first check to see if the value is numeric
                                try:
                                    val_processed = float(value)
                                except:
                                    # Store that they had a value which wasn't able to be converted to a float
                                    val_processed = None
                            # update the list row dict
                            row_dict[i] = val_processed

                    sql = "INSERT into pathway_list_entry (list_id, path_id, name, p_value, z_score, comments) VALUES (%s, %s, %s, %s, %s, %s);"
                    cursor.execute(sql,(list_id, path_id, row_dict['name'], row_dict['p_value'], row_dict['z_score'], ['comments'],))

        # Maybe do a check to test the length of the data added is the same
        # as the size of the list

        # Make the name and id the list name
        list_path_name = self.name + "_" + str(list_id)
        # if the dataet was able to be added completely
        new_file_path = os.path.join(PATH, '%s.tsv' % list_path_name)
        os.rename(self.path, new_file_path)

        # Need to update the filepath in the database
        sql = "UPDATE pathway_lists SET raw_filepath=%s where id=%s;"
        cursor.execute(sql,(new_file_path, list_id,))

        cursor.close()
        conn.commit()
        conn.close()

        self.path = new_file_path
        return "success"


    def get_processed_pathway_list(self, list_id):
        # Returns a list the user has chosen to display on a pathway
        # Check ds_id an int
        if not isinstance(list_id, int) or list_id < 0:
            self.error = True
            return error_codes[ID]

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "SELECT name, notes, private from pathway_lists WHERE id = %s;"

        cursor.execute(sql,(list_id,))
        list_metadata = cursor.fetchall()

        # Now we need the data
        sql = "SELECT * from pathway_list_entry WHERE list_id = %s;"

        cursor.execute(sql,(list_id,))
        list_data = cursor.fetchall()

        cursor.close()
        conn.close()

        if (len(list_metadata) != 1):
            self.error = True
            return "Couldn't retrieve list."


        return {'metadata': list_metadata[0], 'data': list_data}

    def get_all_processed_pathway_lists_for_user(self, uid):
        # Gets all the lists for a user
        # low level check
        if not isinstance(uid, int) or uid < 0:
            return error_codes[ID]

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        sql = "select l.id, name, owner_id, owner_type, notes, private from group_users as gu left join pathway_lists as l on (gu.group_id = l.owner_id and l.owner_type = 'group' and gu.user_id = %s) order by owner_type;"
        cursor.execute(sql,(uid,))

        # We want all the lists that are available just to the user
        # and all the lists which are available to the user within the group

        list_groups = cursor.fetchall()

        sql = "select id, name, owner_id, owner_type, notes, private from pathway_lists where (owner_id = %s and owner_type ='user') or private = 'f';"
        cursor.execute(sql,(uid,))

        list_users = cursor.fetchall()

        cursor.close()
        conn.commit()
        conn.close()

        # Parse the lists group and user information and return a combined dict
        return self.parse_db_user_group_lists(list_groups, list_users)


    def parse_db_user_group_lists(self, list_groups, list_users):
        # want to parse the lists to a dictionary so we can pass it easily
        # to the templates
        user_list_dict = {}
        for l in list_groups:
            if l[0] != None:
                list_dict = {}
                list_dict['name'] = l[1]
                list_dict['owner_id'] = l[2]
                list_dict['owner_type'] = l[3]
                list_dict['notes'] = l[4]
                if (l[5] == 't'):
                    list_dict['private'] = "private"
                else:
                    list_dict['private'] = "public"

                user_list_dict[l[0]] = list_dict # 0 contains list_id
              # note if the user has been given access to a list for both
              # group and user, the user access will override it


        for l in list_users:
            if l[0] != None:
                list_dict = {}
                list_dict['name'] = l[1]
                list_dict['owner_id'] = l[2]
                list_dict['owner_type'] = l[3]
                list_dict['notes'] = l[4]
                if (l[5] == 't'):
                    list_dict['private'] = "private"
                else:
                    list_dict['private'] = "public"

            user_list_dict[l[0]] = list_dict

        return user_list_dict

    


    def delete_pathway_list(self, list_id):
        # Low level check
        if not isinstance(list_id, int) or list_id < 0:
            self.error = True
            return error_codes[ID]

        # first we want to delete all the data associated with the list

        conn = psycopg2.connect(psycopg2_conn_string)
        cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        # Get the list so we can delete it
        sql = "SELECT raw_filepath from pathway_lists WHERE id = %s;"

        cursor.execute(sql,(list_id,))
        list_metadata = cursor.fetchall()

        # Get the raw file path
        list_path_name = list_metadata[0][0]

        # deleting all the columns from the data table which contain the values
        sql = "DELETE FROM pathway_list_entry WHERE list_id = %s;"
        cursor.execute(sql,(list_id,))

        # deleting the metadata
        sql = "DELETE FROM pathway_lists WHERE id = %s;"
        cursor.execute(sql,(list_id,))

        # Test it has been deleted
        sql = "SELECT * from pathway_lists WHERE id = %s;"
        cursor.execute(sql,(list_id,))
        list_metadata = cursor.fetchall()

        cursor.close()
        conn.commit()
        conn.close()

        # Delete the list from the server
        os.remove(list_path_name)

        # Check it was removed
        if list_metadata:
            self.error = True
            return error_codes[ID]

        return "success"


