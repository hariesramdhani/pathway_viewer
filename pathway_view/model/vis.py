from pathway_view.model.pathways import PathwayModel
from pathway_view.model.datasets import DatasetModel
from pathway_view.model.base import BaseModel
from pathway_view.config import *

class VisModel(BaseModel):

    def __init(self):
        self.pathway = None
        self.dataset = None
        self.dictionary = None
        self.error = None


    def get_pathway_and_dataset(self, data_dict): 
        pathway_model = PathwayModel()
        dataset_model = DatasetModel()
        # Gets the pathway
        # Gets the dataset
        # If the dataset hasn't been verified
        #   Verified means there is a bigg id assigned to each value
        #   Under the metadata tag
        # Get the name dict 

