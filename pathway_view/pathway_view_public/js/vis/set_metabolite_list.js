/**
 * Sets the metabolite list and adds it to the stored data
 * so we can perform actions on it at a later time.
 */

/**
 * Toggles the modal display hides or displays the info
 */
var toggle_modal_display = function(element_id) {
    if (element_id == 'stats-container') {
        update_stats_table(pathway);
    }

    var element = document.getElementById(element_id);
    var display = 'block'
    if (element.style.display == 'block') {
        element.style.display = 'none';
        display = 'none'
    } else {
        element.style.display = 'block';
    }
    if (element_id.split("-")[0] == "actions") {
        document.getElementById("actions-options").style.display = display; 
        if (display == 'block') {
            document.getElementById("action-container").style.height = "950px";
        } else {
            document.getElementById("action-container").style.height = "500px";
        }
    }
}




/**
 * Adds the metabolite list information to the modal if the user wants it
 */
var add_metabolite_list_info_to_modal = function(node_id) {
    var node = pathway.data.pathway.nodes[node_id];
    if (node != undefined && pathway.data.metabolite_list != 'na' && pathway.data.metabolite_list.data != undefined) {
        var info = pathway.data.metabolite_list.data[node['chebi_id']];

        if (info != undefined) {
            for (var info_name in info) {
                // Already have the chebi id don't need it
                if (info_name != 'chebi id') {
                    var info_name_clean = info_name.replace(/ /g, "");
                    document.getElementById(info_name_clean).innerHTML = info[info_name];
                }
            }
        } else {
            // Want to clear the info from the last time
            for (var header in options.metabolite_list.metabolite_list_header) {
                document.getElementById(options.metabolite_list.metabolite_list_header[header]).innerHTML = "";
            }
        }
    }
}


var tooltip_mapping_info = d3.select("body").append("div") 
    .attr("class", "tooltip")
    .style("color", "black")
    .style("background-color", "white")
    .style("border-radius", "7px")            
    .style("opacity", 0);



/**
 * Processes a metabolite list for displaying the identification parameters
 * 
 * Updates the class name and makes a tooltip.
 */
var parse_metabolite_list_to_node_id = function(node, info, data_overlay) {
    // for the information we want to add these values to the nodes id
    // so that we can easily select on them
    //
    /*    for (var i in info) {
        var position = options.class_names[i];
        var value = info[i];
        // need to convert the value to a string to make it easier to
        // use for the identifier
        if (value < 0) {
            value = "n" + (-1 * value);
        } else {
            value = "p" + value;
        }
        update_node_class_name(node.node_id, position, value, ['n', 'g']);
    }
    var x = node.x;
    var y = node.y;
    var tmp = {};
    tmp.x = x;
    tmp.y = y;
    tmp.node_id = node.node_id;
    tmp.info = info;
    data_overlay.push(tmp);
    */
    // Want to add this back to the nodes so we can acess it in the modal
    pathway.data.pathway.nodes[node.node_id].metabolite_list_info = info;
    node.metabolite_list_info = info;
    d3.select("#outside-" + node.node_id).attr("fill", "#95D7FF");
    
    pathway.data.node_ids_with_metabolite_list.push(node.node_id);

    return data_overlay;
}

/**
 * Highlight the metabolite list based on an attribute
 */

var highlight_metabolite_on_list_value = function(list_attribute, section_to_hightlight, pos_colour, neg_colour) {
    // For each of the metabolite nodes with a list value
    // highlight green it if it is > 0 otherwise red for < 0
    for (var node_id in pathway.data.node_ids_with_metabolite_list) {
        var node_id = pathway.data.node_ids_with_metabolite_list[node_id];
        if (pathway.data.pathway.nodes[node_id].metabolite_list_info[list_attribute] > 0) {
            d3.select(section_to_hightlight + node_id).attr("fill", pos_colour);//"#40EF9D");
        } else if (pathway.data.pathway.nodes[node_id].metabolite_list_info[list_attribute] <= 0){
            d3.select(section_to_hightlight + node_id).attr("fill", neg_colour);//"#F06893");
        }
    }
}

/**
 * Add the metabolite list circles.
 *
 */
var draw_metabolite_list_feature = function(data_overlay) {
    // Now we want to add circles with the overlay features
    var svg = options.svg;
    svg.selectAll("dot")
        .data(data_overlay)
        .enter().append("circle")
                .attr("r", options.graph.outer_circle_radius)
                .attr("cx", function(d) {
                            return d.x - options.graph.margin.left;
                })
                .attr("cy", function(d) {
                            return d.y;
                })
                .attr("stroke", options.metabolite_list.stroke)
                .attr("stroke-width", options.metabolite_list.stroke_width)
                .attr("fill", options.metabolite_list.fill)
                .attr("opacity", options.metabolite_list.opacity)
                .on("mouseover", function(d) {
                    var html = "<table class='table'> <thead> <tr> <th>" +
                            name + "</th></tr>"
                        + "<tr> <th> Name </th> <th> Value </th> </tr> </thead> <tbody> ";
                    // For each metabolite list information element we
                    // want to display this in the tooltip
                    for (var info_name in d.info) {
                        html += "<tr> <td> " + info_name + " </td>" + 
                                "<td> " + d.info[info_name] + " </td> </tr>"
                    }
                    html += "</tbody> </table>"
                    tooltip_mapping_info.transition()       
                        .duration(50)      
                        .style("opacity", .9);      
                    tooltip_mapping_info.html( html)
                        .style("left", (d3.event.pageX) + "px")     
                        .style("top", (d3.event.pageY - 28) + "px");
                })                  
                .on("mouseout", function(d) {       
                    tooltip_mapping_info.transition()        
                        .duration(50)      
                        .style("opacity", 0);   
                });
}

/**
 * ----------------------------------------------------------------------------------
 *              Total actions
 *  ---------------------------------------------------------------------------------
 */



/**
 * -----------------------------------------------------------------------------------
 *              Metabolite list actions
 * -----------------------------------------------------------------------------------
 */
var highlight_less_than_metric_value = function(value, metric) {
    var total_number_of_metabolites = 0;
    for (var list_name in stored_data.metabolite_lists) {
        var metabolite_list = stored_data.metabolite_lists[list_name];

        for (var chebi_id in metabolite_list.data) {
            var metric_value = metabolite_list.data[chebi_id][metric];
            if (metric_value < value) {
                var nodes = options.neutral_chebi_to_specific_biggs[chebi_id];
                for (var n in nodes) {
              
                }  
            }
        }
    }
}
