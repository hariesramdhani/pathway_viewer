//http://bl.ocks.org/syntagmatic/raw/3299303/

var upload_button = function(load_id, callback) {

    var uploader = document.getElementById(load_id);
    var reader = new FileReader();

    reader.onload = function(e) {
        var contents = e.target.result;
        callback(contents);
    };

    uploader.addEventListener("change", handleFiles, false);  

    function handleFiles() {
        d3.select("#table").text("loading...");
        var file = this.files[0];
        reader.readAsText(file);
    };

}


var parse_header = function(tsv) {
    // Need to do a for loop, if we just take the keys it won't 
    // give the correct ordering
    var header = tsv.split(/\r\n|\r|\n/g);
    header = header[0];
    header = header.split('\t');    
    return header; 
}

var load_first_line = function(tsv) {
    var data = d3.tsv.parse(tsv);
    var header = parse_header(tsv);
    var dropdowns = document.getElementsByClassName('header_selection');
    for (var dropdown in dropdowns) {
        // We get either an int or the name of the dropdown, we only want to
        // append for one (we have both)
        if (!isNaN(parseInt(dropdown))) {
            for (var h in header) {
                var option = document.createElement("option");
                option.value = h; // Store the column indicie as the value
                option.text = header[h];
                var curr_dropdown = dropdowns[dropdown];
                curr_dropdown.appendChild(option);
            };
        };
    };
    

}

upload_button("upload", load_first_line);
