function invert() {
    var invert = document.getElementById('invert').innerHTML;
    if (invert == 'dark') {
        dark();
        document.getElementById('invert').innerHTML = "light";
    } else {
        light();
        document.getElementById('invert').innerHTML = "dark";
    }
}
var white = "#fafeff"
var black = "#24232d"

function light() {
    document.body.style.backgroundColor = white;
    document.body.style.color = black;
    try {
        options.svg.selectAll('text.nodetext').attr('fill', black);
        options.svg.selectAll('text.labeltext').attr('fill', black);
    } catch (err) {
        // Means there is no vis up
    }
}


function dark() {
    document.body.style.backgroundColor = black;
    document.body.style.color = white;
    try {
        options.svg.selectAll('text.nodetext').attr('fill', white);
        options.svg.selectAll('text.labeltext').attr('fill', white);    
    } catch (err) {
        // Menas no vis its fine
    }
}
