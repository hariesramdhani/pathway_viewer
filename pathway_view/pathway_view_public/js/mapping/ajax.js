/**
 * Saves the users mapping to the database.
 *
 * Does this by prompting the user to save when they click
 * to make a new mapping or close the page.
 */
var save_mapping = function () {
    // Get the values of each of the form elements.
    var data = {
        'filename': document.getElementById('filename').value,
        'access_options': document.getElementById('access_options').value,
        'owner_type': document.getElementById('owner_options').value,
        'notes': document.getElementById('notes').value,
        'group_name': document.getElementById('group_name').value,
    };

    jQuery.ajax({
        url     : '/vis/get_dataset_data',
        type    : 'POST',
        data    : data,
        dataType: 'json',
        success : function(response) {
            console.log(success);
        }
    }

}
