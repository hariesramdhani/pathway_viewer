import sys
sys.path.insert(0, '/home/arianem/var/www/html/pathway_viewer/pathway_viewer')
from config import *

sys.path.insert(0, '/home/arianem/var/www/html/pathway_viewer/pathway_viewer/model')
from auth import AuthModel

def auth_model():
    print("Test: add new user.")

    auth = AuthModel()
    full_name = "New user"
    email = "newuser@gmail.com"
    auth.full_name = full_name
    auth.email = email
    user = auth.add_new_user()

    assert user[NAME] == full_name
    assert user[EMAIL] == email
    assert user[ROLE] == 'general'
    uid = int(user[ID])

    print("---------------------------")
    print("Test: change user password.")

    code = user[PASSWORD]
    password = "cakesapiples"
    auth.password = password
#    user = auth.register(code)
    
#    print(email, code, password, auth.error)
#    assert user[NAME] == full_name
#    assert user[EMAIL] == email
#    assert user[ROLE] == 'general'

    print("---------------------------")
    print("Test: user login.")

#    user = auth.login()
#    assert auth.error == None

    print("---------------------------")
    print("Test: create group.")
    
    auth.uid = uid
    role = 'admin'
    auth.role = role
    name = 'some group'
    group = auth.create_group(name)

    assert auth.error == None
    group_id = group[ID]

    print("---------------------------")
    print("Test: add user to group.")

    # Need to create a new user to add to this group
    auth2 = AuthModel()
    full_name2 = "group user"
    email2 = "groupuser@gmail.com"
    auth2.full_name = full_name2
    auth2.email = email2
    user2 = auth2.add_new_user()
    uid2 = user2[ID]

    success = auth.add_user_to_group(email2, name)
    assert auth.error == None
    
    print("---------------------------")
    print("Test: add remove user from group.")

    success = auth.remove_user_from_group(email2, name)
    assert auth.error == None

    print("---------------------------")
    print("Test: remove group.")

    # re add the user to the group so we can check that
    # it removes group_users correctly
    success = auth.add_user_to_group(email2, name)

    group = auth.delete_group(int(group_id))

    assert auth.error == None
    
    print("---------------------------")
    print("Test: remove user.")

    # remove the second user first
    # Then remove the first user
    success = auth.delete_user(int(uid2))
    assert auth.error == None

    
    success = auth.delete_user(int(uid))
    assert auth.error == None


    
    
auth_model()     
