from pathway_view.config import *

from psycopg2.extensions import AsIs

"""

from https://stackoverflow.com/questions/29461933/insert-python-dictionary-using-psycopg2
 
song = {
    'title': 'song 1',
    'artist': 'artist 1'
}

columns = song.keys()
values = [song[column] for column in columns]

insert_statement = 'insert into song_table (%s) values %s'

    # cursor.execute(insert_statement, (AsIs(','.join(columns)), tuple(values)))
     print cursor.mogrify(insert_statement, (AsIs(','.join(columns)), tuple(values)))

prints: insert into song_table (artist,title) values ('artist 1', 'song 1')

"""

    
def test_get(sql, tuple_dict):

    columns = tuple_dict.keys()
    values = [tuple_dict[column] for column in columns]

    conn = psycopg2.connect(psycopg2_conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute(sql,((AsIs(','.join(columns)), tuple(values),)))

    db_val = cursor.fetchall()

    return db_val

def test_update(sql, tuple_dict):
    conn = psycopg2.connect(psycopg2_conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql = "UPDATE users SET password = md5(%s) WHERE email = %s;"

    cursor.execute(sql,(uid, group[0][ID],))
    cursor.close()
    conn.commit()
    conn.close()


def test_delete(sql, tuple_dict):
    conn = psycopg2.connect(psycopg2_conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql = "INSERT INTO group_users (user_id, group_id) VALUES (%s, %s);"
    cursor.execute(sql,(uid, group[0][ID],))
    cursor.close()
    conn.commit()
    conn.close()

def test_insert(sql, tuple_dict):
    conn = psycopg2.connect(psycopg2_conn_string)
    cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    sql = "INSERT INTO group_users (user_id, group_id) VALUES (%s, %s);"
    cursor.execute(sql,(uid, group[0][ID],))
    cursor.close()
    conn.commit()
    conn.close()

print(test_get("SELECT * FROM users WHERE email = %s AND password=md5(%s);", {'email':'ariane2014m@gmail.com', 'password':'camille23'}))
