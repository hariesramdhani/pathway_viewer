from pathway_view.config import *

from pathway_view.model.auth import AuthModel
from pathway_view.model.base import BaseModel


def set_user_role(role):
    base = BaseModel()

    uid = int(input("Please enter the user ID: "))

    print("You entered, user id: ", uid, " to be changed to ", role, ".")
    selection = input("(y/n) to continue: ")

    if selection.lower() != "y":
        print("Wrong selection. Please try again.")
        return None

    sql_update = "UPDATE users set role = %(role)s where id = %(uid)s;"
    data_update = {'role':role , 'uid': uid}

    return_val = base.perform_db_query(sql_update, data_update, True, False)
    
    auth = AuthModel()
    user = auth.get_user_by_uid(uid)

    print("User: ", user[NAME], " id: ", uid, " now has role: ", role)



def add_user_to_group():
    base = BaseModel()

    uid = int(input("Please enter the user ID: "))

    group_id = int(input("Please enter the group ID: "))
    print("You entered, user id: ", uid, " to be added to group: ", group_id)
    selection = input("(y/n) to continue: ")

    if selection.lower() != "y":
        print("Wrong selection. Please try again.")
        return None

    sql_update = "INSERT INTO group_users (user_id, group_id) VALUES(%(user_id)s, %(group_id)s);"
    data_update = {'user_id':uid , 'group_id': group_id}

    return_val = base.perform_db_query(sql_update, data_update, True, False)

    auth = AuthModel()
    user = auth.get_user_by_uid(uid)

    print("User: ", user[NAME], " id: ", uid, " has been added to group: ", group_id)



def create_group():

    uid = int(input("Please enter your user ID (must have admin privleges): "))

    group_name = input("Please enter the group name: ")

    print("You entered, user id: ", uid, " to create group: ", group_name)
    selection = input("(y/n) to continue: ")

    if selection.lower() != "y":
        print("Wrong selection. Please try again.")
        return None

    auth = AuthModel()
    auth.error = None
    user = auth.get_user_by_uid(uid)
    auth.role = user[ROLE]
    auth.uid = uid

    group = auth.create_group(group_name)

    print(group, auth.error)
    print("Group: ", group_name, "has been created: ", group)


def delete_group():

    uid = int(input("Please enter your user ID (must have admin privleges): "))

    group_id = int(input("Please enter the group ID: "))
    print("You entered, user id: ", uid, " will delete group: ", group_id)
    selection = input("(y/n) to continue: ")

    if selection.lower() != "y":
        print("Wrong selection. Please try again.")
        return None
    
    auth = AuthModel()

    auth.error = None
    user = auth.get_user_by_uid(uid)
    auth.role = user[ROLE]
    auth.uid = uid

    group = auth.delete_group(group_id)
    print("Group deleted:", group)


def choose_action():
    action = int(input("Select: \n 1 to set a users role to admin \n 2 to set a users role to general\n 3 to create a group \n 4 to add a user to a group \n 5 to delete a group \n Please enter your choice: "))

    if action == 1:
        return set_user_role('admin')

    if action == 2:
        return set_user_role('general')

    if action == 3:
        return create_group()

    if action == 4:
        return add_user_to_group()
    
    if action == 5:
        return delete_group()

    print("Incorrect response, terminating.")
    return None


choose_action()

